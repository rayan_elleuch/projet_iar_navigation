# -*- coding: utf-8 -*-
"""
Created on Thu Dec 11 21:46:34 2014

@author: rAyyy
"""
from __future__ import division
import numpy
import random
from Tkinter import *

from scipy.stats import norm

############################################################
# DEFINITIONS DES DIFFERENTES CLASSES
############################################################

class Mouse: # Definition de notre classe Personne
    
    def __init__(self, I,J): 
        self.i = I 
        self.j = J
        self.activity = 0
        self.radius = (column_width * zoom)/2

        # PosY = bordure a l'origine + zoom * ( hauteur d'une case * rangee i + ( hauteur d'une case / 2 ) )
        self.PosY = edge_distance + zoom * ( row_height * self.i + row_height/2)
        # PosX = bordure a l'origine + zoom * ( largeur d'une case * colonne j + ( largeur d'une case / 2 ) )
        self.PosX = edge_distance + zoom * ( column_width * self.j + column_width/2)  
        
        self.tkinter = Canevas.create_oval(self.PosX - self.radius, self.PosY - self.radius, self.PosX + self.radius, self.PosY + self.radius, width = zoom, outline = 'black', fill = myred)

    
    def move_up(self):
        if self.i-1 !=-1:
            if g[self.i-1,self.j]!=0:
                self.i-=1
                self.PosY = edge_distance + zoom * ( row_height * self.i + row_height/2)
                Canevas.coords(self.tkinter, self.PosX - self.radius, self.PosY - self.radius, self.PosX + self.radius, self.PosY + self.radius)
            #si on n'arive pas a ce déplacer dans ce sens on diminue le poid de q vers d de cette action
            else:
                current_state.d[0].parents_weigh[2]*=0.5
        else:
            current_state.d[0].parents_weigh[2]*=0.5
            
    def move_down(self):
        if self.i+1 != columnsNb:
            if g[self.i+1,self.j]!=0:
                self.i+=1
                self.PosY = edge_distance + zoom * ( row_height * self.i + row_height/2)
                Canevas.coords(self.tkinter, self.PosX - self.radius, self.PosY - self.radius, self.PosX + self.radius, self.PosY + self.radius)
            #si on n'arive pas a ce déplacer dans ce sens on diminue le poid de q vers d de cette action
            else:
                current_state.d[1].parents_weigh[2]*=0.5
        else:
            current_state.d[1].parents_weigh[2]*=0.5
            
    def move_right(self):
        if self.j+1 != linesNb:
            if g[self.i,self.j+1]!=0:
                self.j+=1
                self.PosX = edge_distance + zoom * ( column_width * self.j + column_width/2) 
                Canevas.coords(self.tkinter, self.PosX - self.radius, self.PosY - self.radius, self.PosX + self.radius, self.PosY + self.radius)
            #si on n'arive pas a ce déplacer dans ce sens on diminue le poid de q vers d de cette action
            else:
                current_state.d[3].parents_weigh[2]*=0.5
        else:
            current_state.d[3].parents_weigh[2]*=0.5
            
    def move_left(self):
        if self.j-1 !=-1:
            if g[self.i,self.j-1]!=0:
                self.j-=1
                self.PosX = edge_distance + zoom * ( column_width * self.j + column_width/2) 
                Canevas.coords(self.tkinter, self.PosX - self.radius, self.PosY - self.radius, self.PosX + self.radius, self.PosY + self.radius)
            #si on n'arive pas a ce déplacer dans ce sens on diminue le poid de q vers d de cette action
            else:
                current_state.d[2].parents_weigh[2]*=0.5
        else:
            current_state.d[2].parents_weigh[2]*=0.5
            

# Définition de notre classe Neuron
class Neuron: 
    
    def __init__(self,mode = 0,par=[] , chi = []): 
    
        self.kind = mode
        '''self.r = random.random()
        self.v = random.random()
        self.i = random.random()'''
        self.r = 0
        self.v = 0
        self.i = 0
        if self.kind == 's':
            self.r = 1
            self.v = 1
            self.i = 1    
        
        self.parents = par
        if self.kind != "place":
            self.parents_weigh = [random.random()]* len(par)
            if mode == "s":
                #on limite les parents au cellule active lors de la création
                parent_temp=list(self.parents)
                for elmt in range(len(self.parents)):
                    temp = self.parents[elmt].r
                    if temp-0.3<=0:
                        parent_temp.remove(self.parents[elmt])
                self.parents=parent_temp
                self.parents_weigh = [random.random()]* len(self.parents)
                for elmt in range(len(self.parents)):
                    temp = self.parents[elmt].r
                    self.parents_weigh[elmt] = temp
                
            elif self.kind == "d":
                for elmt in range(len(par)):
                    self.parents_weigh[elmt] = 1
        '''self.childs= chi
        self.childs_weigh = [0]* len(chi)'''
        
        Neuron_population.append(self)
        
    #
    def transfert_fct(self, entry): #differents types de fonction de transfert
        if self.kind ==1:
            return entry * 1.1**(1-0) #0 = phi
        else :
            return entry
    
    # MAJ de i (eq3)
    def update_input(self,dist='?'):
        if self.kind == "d": # cf eq pour les neuronne d (eq 4)
            temp= max(self.parents[0].r,self.parents[1].r)
            self.i= temp * self.parents[2].r * self.parents_weigh[2]
            
        elif self.kind == "place": # input depent de lemplacement de la souris
            
            zero=1
            one=0.5
            rayon = 10
            a= 1.0 / zero
            b= 1.0 / one
            c=(b-a)*(1.0/rayon)
            
            
            dist = distance(mouse.PosX,self.parents.posx,mouse.PosY,self.parents.posy)
            input_h=norm.pdf(dist,0,20)
            input_m=norm.pdf(0,0,20)
            #self.i = 1.0/(a+dist*c)
            self.i=input_h*(1.0/input_m)
        else: #en général (eq 3)
            max_val = 0
            max_elmt=0
            for elmt in range(len(self.parents)):
                
                temp = self.parents_weigh[elmt] * self.parents[elmt].r
                if temp > max_val:
                    max_val=temp
                    max_elmt=elmt
            self.i= max_val
    
    # MAJ de v  (eq2)        
    def update_potential (self):
        #print "top",self.v
        temp=  (- self.v + self.i)*(1.0/10.0) + self.v 
        #print self.v
        if temp < 0:
            temp=0
        elif temp >1:
            temp=1
        self.v = temp
    
    # MAJ de r (eq1) 
    def update_discharge(self):
        mu = random.uniform(-0.1,0.1)
        temp=self.transfert_fct(self.v + mu)
        if temp < 0:
            temp=0
        elif temp >1:
            temp=1
        self.r = temp
        
    #MAj de wsh (eq8)
    def update_weigh_s(self):
        if self.kind=="s":
            for elmt in range(len(self.parents)):
                    temp = self.parents[elmt].r
                    self.parents_weigh[elmt] +=0.2*self.r*(temp-self.parents_weigh[elmt])

# Définition de notre classe cortical network
class Column_network: 
    
    def __init__(self, X,Y): # Notre méthode constructeur
        self.posx = X
        self.posy = Y
        self.activity = 0
        self.radius = 5
        self.tkinter = Canevas.create_oval(X-self.radius,Y-self.radius,X+self.radius,Y+self.radius,width=1,outline='black',fill=mygreen)

        #neuronne s
        self.s= Neuron(par=Place_Cells_population,mode="s")
        
        #neuronne q
        self.q=[0]*5
        for i in range(5):
            self.q[i]= Neuron(par=[],mode="q") #haut droite bas gauche rien
            
        #neuronne v
        self.v= Neuron(par=self.q,mode="v")

        #si il y a déja une colonne qui a été creer et qui est active on la relie à cette nouvelle colonne (elles sont voisines)
        if current_state!=0:
            #on relie le neuronne p de cette nouvelle colonne aux cellule d de la colonne active en ce moment (wpd)
            self.p= Neuron(par=current_state.d,mode="p")
            #self.p=Neuron(mode="p",par=[])
            for i in range(5):
                #on relie chaque neuronne q de la colonne active en ce momen à la cellule c de cette nouvelle colonne (wqv)
                current_state.q[i].parents.append(self.v)
                current_state.q[i].parents_weigh.append(0.1)
        else:
            #si il n'y a pas detat precedent on ne met pas de parent
            self.p=Neuron(mode="p",par=[])
        
        #neuronne d
        self.d = [0]*5
        for i in range(5):
            self.d[i]=Neuron(par=[self.s,self.p,self.q[i]],mode="d")  #haut droite bas gauche rien
            
# Calcul de la distance
def distance(xi,xii,yi,yii):
    sq1 = (xi-xii)*(xi-xii)
    sq2 = (yi-yii)*(yi-yii)
    return math.sqrt(sq1 + sq2)
    
# Définition de notre classe HP
class Place_cell(Neuron): 
    def __init__(self, X,Y): # Notre méthode constructeur
        Neuron.__init__(self,mode="place",par=self)
        self.posx = X
        self.posy = Y
        self.radius = 2############ a voir
        self.tkinter = Canevas.create_oval(X-self.radius,Y-self.radius,X+self.radius,Y+self.radius,width=1,outline='black',fill=myyellow)
        #self.neurone=Neuron() 



###################################################################################################


############################################################
# DEFINITIONS DES FONCTIONS
############################################################


# draw_checkerboard() : dessine le damier       
def draw_checkerboard():
    for i in range(linesNb+1):
        ni=zoom * row_height * i + edge_distance # on demarre avec un ecart au bord de edge_distance, on doit donc ne pas oublier cet ecart pour chaque ligne ajoutee
        Canevas.create_line(edge_distance, ni, Largeur - edge_distance,ni)
        
    for j in range(columnsNb+1):
        nj=zoom * column_width * j + edge_distance
        Canevas.create_line(nj, edge_distance, nj, Hauteur - edge_distance)


# buildGraphicalInterface(g,linesNb,columnsNb) : construction du labyrinthe et des cellules de lieu
def buildGraphicalInterface(g,linesNb,columnsNb):
    g = configure_g(g) 
       
    for i in range(linesNb):
        for j in range(columnsNb):
            y =zoom * row_height * i + edge_distance
            x =zoom * column_width * j  +edge_distance
            
            if g[i,j] == 0:
                Canevas.create_rectangle(x, y, x + zoom * edge_distance, y + zoom * edge_distance, fill=myblack)
            else:                    
                create_place_cells(x, y, x + zoom * column_width, y + zoom * row_height,1,1)


# action réalisé à chaque pas de temps    
def update():  
    if current_state!=0:
        #print les valeur de d de current state
        for d in current_state.d:
            print d.parents_weigh

    # Update all neurone
    for cell in Neuron_population:
        cell.update_input()
        cell.update_potential()
        cell.update_discharge()
    
    # Verifie s'il y suffisament de cellules de lieux active (eq5)
    stable_bool = test_stability()    
    
    # On regarde les neurones d de chaque colonne de population C1
    motor = [0]*5
    for colonne in C1_population:
        for i in range (5):
            motor[i]+=colonne.d[i].r
    
    # Si stable (si (eq5) verifiee), on calcule (eq6)
    if stable_bool:
        # On regarde s'il y a une colonne de neurone C1 relier à la zone où est actuellement la souris
        somme = 0 # correspond a la somme de l'equation 6
        count=0
        maxi=0
        c_maxi =0
        
        # Application de (eq6)        
        for colonne in C1_population:
            Canevas.itemconfig(colonne.tkinter,fill=mygreen )
            count+=1
            Canevas.tag_raise(colonne.tkinter)
            temp= colonne.s.r
            #print temp
            if temp > maxi:
                maxi=temp
                c_maxi= colonne
            if temp - 0.3>0:
                somme+=1
                
        # on colorie la colonne la plus active en rose et cette colonne devient current_state
        if c_maxi !=0:
            global current_state
            
            mycolor = '#%02x%02x%02x' % (250, 150, 150) 
            Canevas.itemconfig(c_maxi.tkinter,fill=mycolor )
            if c_maxi != current_state:
                # Changement d'état, c'est la que l'on doit faire l'échange d'information entre 2 colonne
                print current_state
                old_state=current_state
                current_state = c_maxi
                if old_state !=0:
                    Canevas.create_line(old_state.posx, old_state.posy, current_state.posx, current_state.posy)
        
        # S'l n'y en a pas on creer une nouvelle collone de neuronne à l'endroit où est la souris
        if somme ==0:
            col=Column_network(mouse.PosX,mouse.PosY)
            C1_population.append(col)
        else:
            current_state.s.update_weigh_s() #eq 8
        
        # Applique le mouvement en fonction des cellule d (motor[i] == colonne.d[i].r)
        maxi = max(motor)
        action = motor.index(maxi) # l'index i correspond au neurone d active
                
        if action == 0:
            mouse.move_up()
        elif action == 1:
            mouse.move_down()
        elif action == 2:
            mouse.move_left()
        elif action == 3:
            mouse.move_right()
            
    
    Mafenetre.after( 1, update)#time_after

def test_stability():
    
    somme=0
    for cell in Place_Cells_population:
        temp = cell.r
        mycolor = '#%02x%02x%02x' % (temp*250, 0, 0) 
        Canevas.itemconfig(cell.tkinter,fill=mycolor )
        if temp - 0.3 > 0:
            somme +=1
    return somme > 6

# create_place_cells(x1,y1,x2,y2,resX,resY): place les cellules de lieux
def create_place_cells(x1,y1,x2,y2,resX,resY):
    deltax= (x2 - x1 )/ resX
    deltay = (y2 - y1 )/ resY
    pasX=x1
    pasY=y1
    #j1= nbcolonnes / 4
    #nj1=zoom*20*j1+20
    while pasX <= x2 and pasY <= y2:
        mean = (pasX,pasY)
        cov = [[5,0],[0,5]]
        x = numpy.random.multivariate_normal(mean,cov,1)
        for things in x:
            Place_Cells_population.append(Place_cell(things[0],things[1]))
        pasX += deltax
        pasY += deltay
        
        
# configure_g(g) : definit les cases du labyrinthe: 1 pour les cases que l'on peut parcourir (cases blaches), 0 sinon (cases noir)
def configure_g(g):       
    for i in range(linesNb):
        # Couloir C6
        idi = int( (columnsNb/4) )
        g[i, idi ] = 1
        g[i, idi + 1] = 1
        
        if i in range(22,38):
            # Couloir C5
            g[i,0] = 1
            g[i,1] = 1
            
        idi = int( (1/20)*linesNb )
        idi2 = int( (3/4)*columnsNb )
        if i in range(idi, idi2):
            # Couloir C7
            g[i, columnsNb-1] = 1
            g[i, columnsNb-2] = 1
    
    idi = int( (columnsNb/4) )
    for j in range(idi):
        idi = int( (1/2)*linesNb )
        # Couloir C2
        g[idi, j] = 1
        g[idi + 1, j] = 1
        
        idi = int( (9/10)*linesNb )
        # Couloir C4
        g[idi, j] = 1
        g[idi + 1, j] = 1

    idi = int( (columnsNb/4) )    
    for j in range(idi,columnsNb):
        idi = int( (1/20)*linesNb )       
        # Couloir C1
        g[idi, j] = 1
        g[idi + 1, j] = 1
        
        idi = int( (7/10)*linesNb )  
        # Couloir C3
        g[idi, j] = 1
        g[idi + 1, j] = 1
    
    return g
  
# Creation de l'interface Tkinter
Mafenetre = Tk()
Mafenetre.title('Labyrinth')

standarDim = 20
standarDimCoeff = 2

# Taille de la grille
linesNb = standarDim * standarDimCoeff
columnsNb = standarDim * standarDimCoeff

# Largeur et hauteur des colonnes et rangees
column_width = 20
row_height = 20

# Distance aux bords du Canvas
edge_distance = 20

zoom = 0.5

Largeur = zoom * column_width * columnsNb + ( 2 * edge_distance )
Hauteur = zoom * row_height * linesNb + ( 2 * edge_distance ) # ( 2 * edge_distance ) :  car on a un edge_distance du bord de gauche (respectivement du bord du haut) et un edge_distance du bord de droite (respectivement du bord du bas)


# Def des couleurs
myred="#D20B18"
mygreen="#25A531"
myblue="#0B79F7"
mygrey="#E8E8EB"
myyellow="#F9FB70"
myblack="#2D2B2B"
mywalls="#5E5E64"
mywhite="#FFFFFF"

#Dictionnaire des differentes actions
actions = dict()
actions['up'] = 0
actions['down'] = 1
actions['left'] = 2
actions['right'] = 3


Canevas = Canvas(Mafenetre, width = Largeur, height =Hauteur, bg =mywhite)

# Creation du labyrinthe et creation de l'interface graphique 
draw_checkerboard()

g = numpy.zeros((linesNb,columnsNb), dtype=numpy.int)

C1_population = []
Place_Cells_population = []
Neuron_population = []

current_state=0 # colonne auquel est le plus relié actuellement la souris
old_state=0# la précedente

buildGraphicalInterface(g,linesNb,columnsNb)

# Widget.pack() : options "d'empaquetage", c'est a dire les proprietes de la zone graphique et ce qui l'entoure
Canevas.pack(padx =5, pady =5)

mouse = Mouse(linesNb - 1, int( (1/4) * columnsNb) )

# Temps pour chaque tour ( de lafonction Tkinter.after() )
time_after = 100


Mafenetre.after( time_after, update )
Mafenetre.mainloop()