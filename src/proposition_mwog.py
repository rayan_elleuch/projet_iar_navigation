# -*- coding: utf-8 -*-
"""
Created on Thu Dec 11 21:46:34 2014

@author: rAyyy
"""
from __future__ import division
import numpy
import random
from Tkinter import *
############################################################
# DEFINITIONS DES DIFFERENTES CLASSES
############################################################

class Mouse: # Definition de notre classe Personne
    
    def __init__(self, I,J): 
        self.i = I 
        self.j = J
        self.activity = 0
        self.radius = (column_width * zoom)/2

        # PosY = bordure a l'origine + zoom * ( hauteur d'une case * rangee i + ( hauteur d'une case / 2 ) )
        PosY = edge_distance + zoom * ( row_height * self.i + row_height/2)
        # PosX = bordure a l'origine + zoom * ( largeur d'une case * colonne j + ( largeur d'une case / 2 ) )
        PosX = edge_distance + zoom * ( column_width * self.j + column_width/2)  
        
        self.tkinter = Canevas.create_oval(PosX - self.radius, PosY - self.radius, PosX + self.radius, PosY + self.radius, width = zoom, outline = 'black', fill = myyellow)

    
    def move_up(self):
        self.i-=1
        PosY = edge_distance + zoom * ( row_height * self.i + row_height/2)
        PosX = edge_distance + zoom * ( column_width * self.j + column_width/2) 
        Canevas.coords(self.tkinter, PosX - self.radius, PosY - self.radius, PosX + self.radius, PosY + self.radius)
        
    def move_down(self):
        self.i+=1
        PosY = edge_distance + zoom * ( row_height * self.i + row_height/2)
        PosX = edge_distance + zoom * ( column_width * self.j + column_width/2) 
        Canevas.coords(self.tkinter, PosX - self.radius, PosY - self.radius, PosX + self.radius, PosY + self.radius)

    def move_right(self):
        self.j+=1
        PosY = edge_distance + zoom * ( row_height * self.i + row_height/2)
        PosX = edge_distance + zoom * ( column_width * self.j + column_width/2) 
        Canevas.coords(self.tkinter, PosX - self.radius, PosY - self.radius, PosX + self.radius, PosY + self.radius)

    def move_left(self):
        self.j-=1
        PosY = edge_distance + zoom * ( row_height * self.i + row_height/2)
        PosX = edge_distance + zoom * ( column_width * self.j + column_width/2) 
        Canevas.coords(self.tkinter, PosX - self.radius, PosY - self.radius, PosX + self.radius, PosY + self.radius)



class Neuron: # Définition de notre classe Neuron
    
    def __init__(self,mode = 0,par=[] , chi = []): # Notre méthode constructeur
        self.r = random.random()
        self.v = random.random()
        self.i = random.random()
        
        self.kind = mode
        
        self.parents = par
        self.parents_weigh = [random.random()]* len(par)
            
        '''self.childs= chi
        self.childs_weigh = [0]* len(chi)'''
        
    def transfert_fct(self, entry): #differennt type de fonction de transfert
        if self.kind ==0:
            return entry
        if self.kind ==1:
            return entry * 1.1**(1-0) #0 = phi
    
    def update_input(self):
        max_val = 0
        max_elmt=0
        for elmt in range(len(self.parents)):
            temp = self.parents_weigh[elmt] * self.parents.r
            if temp < 0:
                temp=0
            elif temp >1:
                temp=1
            if temp > max_val:
                max_val=temp
                max_elmt=elmt
        self.i= max_val
        
    def update_potential (self):
        #print "top",self.v
        temp=  (- self.v + self.i)*(1.0/10.0) + self.v 
        #print self.v
        if temp < 0:
            temp=0
        elif temp >1:
            temp=1
        self.v = temp
    def update_discharge(self):
        mu = random.uniform(-0.1,0.1)
        temp=self.transfert_fct(self.v + mu)
        if temp < 0:
            temp=0
        elif temp >1:
            temp=1
        self.r = temp



class Place_cell(Neuron): # Définition de notre classe HP

    
    def __init__(self, X,Y): # Notre méthode constructeur
        Neuron.__init__(self)
        self.posx = X
        self.posy = Y
        self.radius = 3
        self.tkinter = Canevas.create_oval(X-self.radius,Y-self.radius,X+self.radius,Y+self.radius,width=1,outline='black',fill=myyellow)
        #self.neurone=Neuron() 


###################################################################################################


############################################################
# DEFINITIONS DES FONCTIONS
############################################################


# draw_checkerboard() : dessine le damier       
def draw_checkerboard():
    for i in range(linesNb+1):
        ni=zoom * row_height * i + edge_distance # on demarre avec un ecart au bord de edge_distance, on doit donc ne pas oublier cet ecart pour chaque ligne ajoutee
        Canevas.create_line(edge_distance, ni, Largeur - edge_distance,ni)
        
    for j in range(columnsNb+1):
        nj=zoom * column_width * j + edge_distance
        Canevas.create_line(nj, edge_distance, nj, Hauteur - edge_distance)


# buildGraphicalInterface(g,linesNb,columnsNb) : construction du labyrinthe et des cellules de lieu
def buildGraphicalInterface(g,linesNb,columnsNb):
    g = configure_g(g) 
       
    for i in range(linesNb):
        for j in range(columnsNb):
            y =zoom * row_height * i + edge_distance
            x =zoom * column_width * j  +edge_distance
            
            if g[i,j] == 0:
                Canevas.create_rectangle(x, y, x + zoom * edge_distance, y + zoom * edge_distance, fill=myblack)
            else:                    
                create_place_cells(x, y, x + zoom * column_width, y + zoom * row_height,2,4)


# action_choice( action ): choix de l'action a effectuee au prochain tour, en fonction de l'etat actuel (PosX et PosY) et de la derniere action effectuee pour arriver dans cet etat ( action )
def action_choice(action, animal):
    
    j = animal.j
    i = animal.i

    #Point de depart
    if int(i/standarDimCoeff) == 19 and int(j/standarDimCoeff) == 5:
        action = 0
    
    #Etat but
    elif int(i/standarDimCoeff) == 0 and int(j/standarDimCoeff) == 5:
        action = 1
        
    #Intersection couloirs C1 et C7
    elif int(i/standarDimCoeff) == 1 and int(j/standarDimCoeff) == 19:
        # Si on allait a droite        
        if action == 3:
            action = 1 # on descend (bas)
        # Sinon si on allait on montait (haut)
        else:
            action = 2 # on va a gauche
            
    #Intersection couloirs C3 et C7
    elif int(i/standarDimCoeff) == 14 and int(j/standarDimCoeff) == 19:
        # Si on allait a droite
        if action == 3:
            action = 0 # on monte (haut)
        # Sinon si on allait descendait (bas)
        else:
            action = 2 # On va a gauche
            
    #Intersection couloirs C2 et C5    
    elif int(i/standarDimCoeff) == 10 and int(j/standarDimCoeff) == 0:
        # Si on allait a gauche
        if action == 2:
            action = 1 # On descend (bas)
        # Sinon si on montait (haut) 
        else:
            action = 3 # On va a droite
            
    #Intersection couloirs C4 et C5 
    elif int(i/standarDimCoeff) == 18 and int(j/standarDimCoeff) == 0: 
        # Si on allait a gauche
        if action == 2:
            action = 0 # On monte (haut)
        # Sinon si on descendait (bas) 
        else:
            action = 3 # On va a droite
            
    # Intersection couloirs C4 et C6
    elif int(i/standarDimCoeff) == 18 and int(j/standarDimCoeff) == 5:
        r = random.random()
        # Couloir C4
        if r < 1.0/3:
            action = 2 # gauche
        # Couloir C6 haut        
        elif r>=(1.0/3) and r < 2.0/3:
            action = 0 # haut
        # Couloir C6 bas
        else:
            action = 1 # bas            
    
    # Intersection couloirs C3 et C6             
    elif int(i/standarDimCoeff) == 14 and int(j/standarDimCoeff) == 5:
        r = random.random()
        # Couloir C3
        if r < 1.0/3:
            action = 3 # droite
        # Couloir C6 haut        
        elif r >= 1.0/3 and r < 2.0/3:
            action = 0 # haut
        # Couloir C6 bas
        else:
            action = 1 # bas
            
    # Intersection couloirs C2 et C6            
    elif int(i/standarDimCoeff) == 10 and int(j/standarDimCoeff) == 5:
        r = random.random()
        # Couloir C2
        if r < 1.0/3:
            action = 2 # gauche
        # Couloir C6 haut        
        elif r>=1.0/3 and r <2.0/3:
            action = 0 # haut
        # Couloir C6 bas
        else:
            action = 1 # bas 

    # Intersection couloirs C1 et C6
    elif int(i/standarDimCoeff) == 1 and int(j/standarDimCoeff) == 5:
        r = random.random()
        # Couloir C1
        if r < 1.0/3:
            action = 3 # droite
        # Couloir C6 haut        
        elif r >= 1.0/3 and r < 2.0/3:
            action = 0 # haut
        # Couloir C6 bas
        else:
            action = 1 # bas

    return action

# apply_action(action) : application de l'action action.    
def apply_action(action, animal):    
    action = action_choice(action, animal)
            
    if action == 0:
        print('haut')
        animal.move_up()
    elif action == 1:
        print('bas')
        animal.move_down()
    elif action == 2:
        print('gauche')
        animal.move_left()
    elif action == 3:
        print('droite')
        animal.move_right()

    #Canevas.coords(Pion, PosX - (row_height * zoom)/2, PosY - (column_width * zoom)/2, PosX + (row_height * zoom)/2, PosY + (column_width * zoom)/2) 

    Mafenetre.after( time_after, apply_action, action, animal )


# create_place_cells(x1,y1,x2,y2,resX,resY): place les cellules de lieux
def create_place_cells(x1,y1,x2,y2,resX,resY):
    deltax= (x2 - x1 )/ resX
    deltay = (y2 - y1 )/ resY
    pasX=x1
    pasY=y1
    #j1= nbcolonnes / 4
    #nj1=zoom*20*j1+20
    while pasX <= x2 and pasY <= y2:
        mean = (pasX,pasY)
        cov = [[50,0],[0,50]]
        x = numpy.random.multivariate_normal(mean,cov,30)
        for things in x:
            Place_Cells_population.append(Place_cell(things[0],things[1]))
        pasX += deltax
        pasY += deltay
        
        
# configure_g(g) : definit les cases du labyrinthe: 1 pour les cases que l'on peut parcourir (cases blaches), 0 sinon (cases noir)
def configure_g(g):       
    for i in range(linesNb):
        # Couloir C6
        idi = int( (columnsNb/4) )
        g[i, idi ] = 1
        g[i, idi + 1] = 1
        
        if i in range(22,38):
            # Couloir C5
            g[i,0] = 1
            g[i,1] = 1
            
        idi = int( (1/20)*linesNb )
        idi2 = int( (3/4)*columnsNb )
        if i in range(idi, idi2):
            # Couloir C7
            g[i, columnsNb-1] = 1
            g[i, columnsNb-2] = 1
    
    idi = int( (columnsNb/4) )
    for j in range(idi):
        idi = int( (1/2)*linesNb )
        # Couloir C2
        g[idi, j] = 1
        g[idi + 1, j] = 1
        
        idi = int( (9/10)*linesNb )
        # Couloir C4
        g[idi, j] = 1
        g[idi + 1, j] = 1

    idi = int( (columnsNb/4) )    
    for j in range(idi,columnsNb):
        idi = int( (1/20)*linesNb )       
        # Couloir C1
        g[idi, j] = 1
        g[idi + 1, j] = 1
        
        idi = int( (7/10)*linesNb )  
        # Couloir C3
        g[idi, j] = 1
        g[idi + 1, j] = 1
    
    return g
  
# Creation de l'interface Tkinter
Mafenetre = Tk()
Mafenetre.title('Labyrinth')

standarDim = 20
standarDimCoeff = 2

# Taille de la grille
linesNb = standarDim * standarDimCoeff
columnsNb = standarDim * standarDimCoeff

# Largeur et hauteur des colonnes et rangees
column_width = 20
row_height = 20

# Distance aux bords du Canvas
edge_distance = 20

zoom = 1

Largeur = zoom * column_width * columnsNb + ( 2 * edge_distance )
Hauteur = zoom * row_height * linesNb + ( 2 * edge_distance ) # ( 2 * edge_distance ) :  car on a un edge_distance du bord de gauche (respectivement du bord du haut) et un edge_distance du bord de droite (respectivement du bord du bas)


# Def des couleurs
myred="#D20B18"
mygreen="#25A531"
myblue="#0B79F7"
mygrey="#E8E8EB"
myyellow="#F9FB70"
myblack="#2D2B2B"
mywalls="#5E5E64"
mywhite="#FFFFFF"

#Dictionnaire des differentes actions
actions = dict()
actions['up'] = 0
actions['down'] = 1
actions['left'] = 2
actions['right'] = 3


Canevas = Canvas(Mafenetre, width = Largeur, height =Hauteur, bg =mywhite)

# Creation du labyrinthe et creation de l'interface graphique 
draw_checkerboard()

g = numpy.zeros((linesNb,columnsNb), dtype=numpy.int)

Place_Cells_population = []

buildGraphicalInterface(g,linesNb,columnsNb)

# Widget.pack() : options "d'empaquetage", c'est a dire les proprietes de la zone graphique et ce qui l'entoure
Canevas.pack(padx =5, pady =5)

mouse = Mouse(linesNb - 1, int( (1/4) * columnsNb) )

# Action initiale de l'etat de depart
init_action = 0

# Temps pour chaque tour ( de lafonction Tkinter.after() )
time_after = 100


Mafenetre.after( time_after, apply_action, init_action, mouse )
Mafenetre.mainloop()